-module(prime_grinder_filter).
-behaviour(gen_server).
-define(SERVER, ?MODULE).

-record(state, {
    q_key :: binary(),
    rs_key :: binary(),
    redis,
    primes
}).

%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------

-export([start_link/6]).

%% ------------------------------------------------------------------
%% gen_server Function Exports
%% ------------------------------------------------------------------

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, handle_continue/2,
         terminate/2, code_change/3]).

-define(LOG(Fmt, Args), io:format(Fmt, Args)).

%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link(RedisHost, RedisPort, RedisDB, QueueKey, ResultSetKey, N) ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [RedisHost, RedisPort, RedisDB, QueueKey, ResultSetKey, N], []).

%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------

init([RedisHost, RedisPort, RedisDB, QueueKey, ResultSetKey, N]) ->
    Primes = collect_primes(N),
    {ok, Conn} = eredis:start_link(RedisHost, RedisPort, RedisDB),
    {ok, #state{redis = Conn, rs_key = ResultSetKey, q_key = QueueKey, primes = Primes}, {continue, ok}}.

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

handle_continue(ok, State = #state{redis = Conn, rs_key = ResultSetKey, q_key = QueueKey, primes = Primes}) ->
    race(Conn, ResultSetKey, QueueKey, Primes),
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------

race(Conn, ResultSetKey, QueueKey, Primes) ->
    {ok, [QueueKey, Value]} = eredis:q(Conn, ["BRPOP", QueueKey, 0]),
    Num = binary_to_integer(Value),
    case sets:is_element(Num, Primes) of
        true ->
            ?LOG("Prime: ~b~n", [Num]),
            eredis:q(Conn, ["SADD", ResultSetKey, Num]);
        false -> ok
    end,
    race(Conn, ResultSetKey, QueueKey, Primes).

collect_primes(N) ->
    IsPrime = fun(_, false) -> false;
        (Prime, X) -> X rem Prime /= 0 andalso X
    end,
    F = fun(X, Primes1) ->
        case sets:fold(IsPrime, X, Primes1) of
            false -> Primes1;
            X -> sets:add_element(X, Primes1)
        end
    end,
    lists:foldl(F, sets:new(), lists:seq(2, N)).

-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

collect_primes_test() ->
    Primes = [2, 3, 5, 7, 11, 13, 17, 19],
    ?assertEqual(Primes, lists:sort(sets:to_list(collect_primes(20)))).

race_test() ->
    meck:new(eredis),
    Parent = self(),
    Q = fun
        (_Conn, ["BRPOP", <<"from">>, 0]) ->
            [Num | Tail] = case get(queue) of
                undefined ->
                    put(primes, []),
                    [<<"2">>, <<"3">>, <<"4">>, <<"5">>, <<"6">>, <<"7">>, <<"8">>];
                [] ->
                    Parent ! {primes, get(primes)},
                    exit(normal);
                Queue1 -> Queue1
            end,
            put(queue, Tail),
            {ok, [<<"from">>, Num]};
        (_Conn, ["SADD", <<"to">>, Num]) ->
            put(primes, [Num | get(primes)])
    end,
    meck:expect(eredis, q, Q),
    spawn(fun() -> race(ok, <<"to">>, <<"from">>, collect_primes(10)) end),
    receive
        {primes, Primes} ->
            io:format(user, "Primes: ~p~n", [Primes]),
            ?assertEqual(Primes, [7, 5, 3, 2])
        after 3000 -> error("race timeout")
    end,
    meck:unload(eredis).

-endif.
