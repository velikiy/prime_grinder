prime_grinder
=====

Prime Grinder

Build
-----

    $ rebar3 compile

Start
-----

    $ rebar3 shell

Test
-----

    $ rebar3 eunit
