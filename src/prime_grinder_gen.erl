-module(prime_grinder_gen).
-behaviour(gen_server).
-define(SERVER, ?MODULE).

-record(state, {
    q_key :: binary(),
    redis,
    n :: non_neg_integer()
}).

-define(DELTA, 333). %% microseconds
-define(LOG(Fmt, Args), io:format(Fmt, Args)).
%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------

-export([start_link/5]).

%% ------------------------------------------------------------------
%% gen_server Function Exports
%% ------------------------------------------------------------------

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, handle_continue/2,
         terminate/2, code_change/3]).

%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link(RedisHost, RedisPort, RedisDB, QueueKey, N) ->
    gen_server:start_link(?MODULE, [RedisHost, RedisPort, RedisDB, QueueKey, N], []).

%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------

init([RedisHost, RedisPort, RedisDB, QueueKey, N]) ->
    {ok, Conn} = eredis:start_link(RedisHost, RedisPort, RedisDB),
    {ok, #state{q_key = QueueKey, n = N, redis = Conn}, {continue, ok}}.

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

handle_continue(ok, State = #state{q_key = QueueKey, n = N, redis = Conn}) ->
    Time = erlang:system_time(microsecond),
    race(Conn, QueueKey, N, Time, 0, 0),
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------

race(Conn, QueueKey, N, NextTime, Step, Qual) ->
    Time = erlang:system_time(microsecond),
    if
        Time >= NextTime ->
            Num = rand:uniform(N - 1) + 1,
            NewStep = Step + 1,
            NewQual = Qual + (NextTime - Time) * (NextTime - Time),
            eredis:q(Conn, ["LPUSH", QueueKey, Num]),
            race(Conn, QueueKey, N, NextTime + ?DELTA, NewStep, NewQual);
        true -> %% soon
            race(Conn, QueueKey, N, NextTime, Step, Qual)
    end.

-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

race_test() ->
    meck:new(eredis),
    Time = erlang:system_time(microsecond),
    NextTime = Time + ?DELTA,
    Parent = self(),
    Q = fun(_Conn, ["LPUSH", <<"test">>, _Num]) ->
        {Step, OldTime, Qual} = case get(data) of
            undefined -> {0, 0, 0};
            Data -> Data
        end,
        Time1 = erlang:system_time(microsecond),
        Delta = Time1 - OldTime - ?DELTA,
        NewData = if 
            Step =< 1 -> {Step + 1, Time1, Qual};
            true -> {Step + 1, Time1, Qual + Delta * Delta}
        end,
        put(data, NewData),
        if
            Step < 1000 -> ok;
            true ->
                Parent ! {qual, math:sqrt(element(3, NewData) / Step)},
                exit(normal)
        end
    end,
    meck:expect(eredis, q, Q),
    spawn(fun() -> race(ok, <<"test">>, 99, NextTime, 0, 0) end),
    receive
        {qual, Qual} ->
            io:format(user, "Qual: ~f~n", [Qual]),
            ?assert(Qual < 100)
        after 3000 -> error("race timeout")
    end,
    meck:unload(eredis).

-endif.
