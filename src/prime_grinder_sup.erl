%%%-------------------------------------------------------------------
%% @doc prime_grinder top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(prime_grinder_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).
-define(APP, prime_grinder).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: #{id => Id, start => {M, F, A}}
%% Optional keys are restart, shutdown, type, modules.
%% Before OTP 18 tuples must be used to specify a child. e.g.
%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
    {ok, RedisHost} = application:get_env(?APP, redis_host),
    {ok, RedisPort} = application:get_env(?APP, redis_port),
    {ok, RedisDB} = application:get_env(?APP, redis_db),
    {ok, ResultSetKey} = application:get_env(?APP, result_set_key),
    {ok, QueueKey} = application:get_env(?APP, queue_key),
    {ok, N} = application:get_env(?APP, n),
    GenArgs = [RedisHost, RedisPort, RedisDB, QueueKey, N],
    FilterArgs = [RedisHost, RedisPort, RedisDB, QueueKey, ResultSetKey, N],
    Gen = #{id => gen, start => {prime_grinder_gen, start_link, GenArgs}},
    Filter = #{id => filter, start => {prime_grinder_filter, start_link, FilterArgs}},
    {ok, {{one_for_all, 0, 1}, [Filter, Gen]}}.

%%====================================================================
%% Internal functions
%%====================================================================
